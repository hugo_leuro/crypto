import "firebase/auth";
import "firebase/firestore";
import firebase from "firebase/compat";

// Initialize Firebase
firebase.initializeApp({
  apiKey: "AIzaSyAk7VXIgZJZ7zSvogEjXYZlzQURupTJxJ4",
  authDomain: "cryptotracker-b9a5a.firebaseapp.com",
  projectId: "cryptotracker-b9a5a",
  storageBucket: "cryptotracker-b9a5a.appspot.com",
  messagingSenderId: "442375837261",
  appId: "1:442375837261:web:a6e9b9cbb6410d0e886e04",
});

const db = firebase.firestore();
const auth = firebase.auth();

// collection references
const usersCollection = db.collection("users");

// export utils/refs
export { db, auth, usersCollection };
