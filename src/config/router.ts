import VueRouter, { RouteRecord, RouterOptions } from "vue-router";
import Vue from "vue";
import { routerOptions } from "@/routes";
import { auth } from "@/firebase";

Vue.use(VueRouter);

class Router {
  private readonly vueRouter: VueRouter;

  constructor(options: RouterOptions) {
    this.vueRouter = new VueRouter(options);

    this.vueRouter.beforeEach((to, from, next) => {
      const requiresAuth = to.matched.some(
        (x: RouteRecord) => x.meta.requiresAuth
      );

      if (requiresAuth && !auth.currentUser) next({ name: "signin" });
      else next();
    });
  }

  getVueRouter(): VueRouter {
    return this.vueRouter;
  }
}

export const createRouter = () => new Router(routerOptions).getVueRouter();
