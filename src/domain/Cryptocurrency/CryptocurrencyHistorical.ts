export interface CryptocurrencyHistorical {
  date: Date;
  price: number;
}
