import { CryptocurrencyHistorical } from "@/domain/Cryptocurrency/CryptocurrencyHistorical";

export interface CryptocurrencyTrack {
  price: number;
  image: string;
  symbol: string;
  marketCap: number;
  historical: CryptocurrencyHistorical[];
}
