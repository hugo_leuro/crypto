export interface CryptocurrencyTransaction {
  date: string;
  type: string;
  cryptoCurrency: string;
  amount: number;
  toCurrency: string;
  toAmount: number;
  baseCurrency: string;
  baseAmount: number;
  baseUsdtAmount: number;
  transactionKind: string;
}

export interface CryptocurrencyTransactions {
  transactions: CryptocurrencyTransaction[];
}
