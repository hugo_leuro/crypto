import { RestCryptocurrency } from "@/secondary/RestAccount/RestCryptocurrencyPrice";

export interface CryptocurrencyRepository {
  getPrice(symbol: string): any;
  getMultiplePrice(symbols: string): Promise<RestCryptocurrency>;
  getMultiplePriceFull(symbols: string): any;
  getListCryptoInformations(symbol: string, limit: number): any;
}
