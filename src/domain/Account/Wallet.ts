export interface Wallet {
  crypto: CryptoWallet[];
}

export interface CryptoWallet {
  logo: string;
  name: string;
  total: number;
  value: number;
  baseValue: number;
}

export const emptyWallet = () => ({
  crypto: [],
});
