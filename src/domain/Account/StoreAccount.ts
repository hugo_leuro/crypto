export interface StoreAccount {
  token: string;
  refreshToken: string;
}
