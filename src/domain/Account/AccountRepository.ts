import { Wallet } from "@/domain/Account/Wallet";
import {
  CryptocurrencyTransaction,
  CryptocurrencyTransactions,
} from "@/domain/Cryptocurrency/CryptocurrencyTransaction";

export interface AccountRepository {
  signIn(email: string, password: string): Promise<boolean>;
  signUp(email: string, password: string): Promise<boolean>;
  saveTransactions(
    cryptocurrencyTransactions: CryptocurrencyTransaction[]
  ): Promise<boolean>;
  getTransactions(): Promise<CryptocurrencyTransactions>;
  getWallet(): Promise<Wallet>;
}
