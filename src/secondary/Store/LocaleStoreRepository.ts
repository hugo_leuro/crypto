import { StoreRepository } from "@/secondary/Store/StoreRepository";
import { Store } from "vuex";
import { StoreAccount } from "@/domain/Account/StoreAccount";

export class LocaleStoreRepository implements StoreRepository {
  private readonly USER: string = "user";

  constructor(private store: Store<any>) {}

  modifyUser(storeAccount: StoreAccount): void {
    this.modifyLocalStorageByTokenKey(this.USER, storeAccount);
  }

  private modifyLocalStorageByTokenKey(tokenKey: string, object: any) {
    this.store.commit(
      "localStorageModule/modify" +
        tokenKey.charAt(0).toUpperCase() +
        tokenKey.slice(1),
      object
    );
  }
}
