export const localStorageModule = () => ({
  namespaced: true,
  state: () => ({
    user: {},
  }),
  getters: {
    user: (state: any) => state.user,
  },
  mutations: {
    modifyUser(state: any, user: any) {
      state.user = user;
    },
  },
});
