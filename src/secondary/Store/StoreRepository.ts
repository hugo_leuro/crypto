import { StoreAccount } from "@/domain/Account/StoreAccount";

export interface StoreRepository {
  modifyUser(storeAccount: StoreAccount): void;
}
