import Vue from "vue";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
import Vuex from "vuex";
import { localStorageModule } from "@/secondary/Store/LocaleStorageModule";

Vue.use(Vuex);

export const createStore = () =>
  new Vuex.Store({
    modules: {
      localStorageModule: localStorageModule(),
    },
  });
