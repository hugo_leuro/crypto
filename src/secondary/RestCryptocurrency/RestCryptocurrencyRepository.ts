import { CryptocurrencyRepository } from "@/domain/Cryptocurrency/CryptocurrencyRepository";
import { AxiosInstance } from "axios";
import firebase from "firebase/compat";
import { RestCryptocurrency } from "@/secondary/RestAccount/RestCryptocurrencyPrice";
import { toCryptocurrencyTrack } from "@/secondary/RestCryptocurrency/RestCryptocurrencyTrack";
import { CryptocurrencyTrack } from "@/domain/Cryptocurrency/CryptocurrencyTrack";
import { toCryptocurrencyHistorical } from "@/secondary/RestCryptocurrency/RestCryptocurrencyHistorical";
import { CryptocurrencyHistorical } from "@/domain/Cryptocurrency/CryptocurrencyHistorical";
import Firestore = firebase.firestore.Firestore;
import Auth = firebase.auth.Auth;

export class RestCryptocurrencyRepository implements CryptocurrencyRepository {
  constructor(
    private firestore: Firestore,
    private auth: Auth,
    private axiosInstance: AxiosInstance
  ) {}

  getPrice(symbol: string): any {
    return this.axiosInstance
      .get(`/price?fsym=${symbol}&tsyms=EUR`)
      .then((data) => console.log(data))
      .catch((error) => {
        console.log(error);
      });
  }

  getMultiplePrice(symbols: string): Promise<RestCryptocurrency> {
    return this.axiosInstance
      .get(`/pricemulti?fsyms=${symbols}&tsyms=EUR`)
      .then((data) => data.data as RestCryptocurrency)
      .catch((error) => {
        throw Error(error);
      });
  }

  getMultiplePriceFull(symbols: string): Promise<CryptocurrencyTrack[]> {
    return this.axiosInstance
      .get(`/pricemultifull?fsyms=${symbols}&tsyms=EUR`)
      .then(async (data) => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        const array: any = Object.values(Object.values(data.data)[0]);
        const result: CryptocurrencyTrack[] = [];
        let historical: CryptocurrencyHistorical[] = [];

        for (const item of array) {
          await this.getListCryptoInformations(item.EUR.FROMSYMBOL).then(
            (data) => {
              historical = data;
              result.push(toCryptocurrencyTrack(item.EUR, historical));
            }
          );
        }
        return result;
      })
      .catch((error) => {
        throw Error(error);
      });
  }

  getListCryptoInformations(
    symbol: string,
    limit = 147
  ): Promise<CryptocurrencyHistorical[]> {
    return this.axiosInstance
      .get(`v2/histohour?fsym=${symbol}&tsym=EUR&limit=${limit}`)
      .then((data) => {
        return data.data.Data.Data.map(toCryptocurrencyHistorical);
      })
      .catch((error) => {
        throw Error(error);
      });
  }
}
