import { CryptocurrencyHistorical } from "@/domain/Cryptocurrency/CryptocurrencyHistorical";

export interface RestCryptocurrencyHistorical {
  time: number;
  open: number;
}

export const toCryptocurrencyHistorical = (
  restCryptocurrencyHistorical: RestCryptocurrencyHistorical
): CryptocurrencyHistorical => ({
  date: new Date(restCryptocurrencyHistorical.time * 1000),
  price: restCryptocurrencyHistorical.open,
});
