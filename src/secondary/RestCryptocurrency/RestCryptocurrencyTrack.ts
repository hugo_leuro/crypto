import { CryptocurrencyTrack } from "@/domain/Cryptocurrency/CryptocurrencyTrack";
import { CryptocurrencyHistorical } from "@/domain/Cryptocurrency/CryptocurrencyHistorical";

export interface RestCryptocurrencyTrack {
  IMAGEURL: string;
  FROMSYMBOL: string;
  PRICE: number;
  MKTCAP: number;
}

export const toCryptocurrencyTrack = (
  restCryptocurrencyTrack: RestCryptocurrencyTrack,
  historical: CryptocurrencyHistorical[]
): CryptocurrencyTrack => {
  return {
    image:
      process.env.VUE_APP_BASE_IMAGE_URL + restCryptocurrencyTrack.IMAGEURL,
    price: restCryptocurrencyTrack.PRICE,
    symbol: restCryptocurrencyTrack.FROMSYMBOL,
    marketCap: restCryptocurrencyTrack.MKTCAP,
    historical: historical,
  };
};
