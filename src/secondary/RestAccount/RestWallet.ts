import { CryptoWallet, Wallet } from "@/domain/Account/Wallet";
import { RestCryptocurrencyPrice } from "@/secondary/RestAccount/RestCryptocurrencyPrice";

export interface RestWallet {
  crypto: RestWalletCrypto[];
  totalSpend: number;
}

interface RestWalletCrypto {
  logo?: string;
  name: string;
  total: number;
}

export const toWallet = (
  restWallet: RestWallet,
  listPrice: RestCryptocurrencyPrice[]
): Wallet => ({
  crypto: restWallet.crypto.map((item, index) =>
    toCryptoWallet(item, listPrice[index])
  ),
});

const toCryptoWallet = (
  restWalletCrypto: RestWalletCrypto,
  price: RestCryptocurrencyPrice
): CryptoWallet => ({
  logo: restWalletCrypto.logo ? restWalletCrypto.logo : "",
  name: restWalletCrypto.name,
  total: restWalletCrypto.total,
  baseValue: price.EUR,
  value: toValue(restWalletCrypto.total, price.EUR),
});

export const getListCryptos = (restWallet: RestWallet): string => {
  const list: string[] = [];

  restWallet.crypto.forEach((item) => {
    list.push(item.name);
  });

  return list.toString();
};

const toValue = (total: number, price: number) => {
  return total * price;
};
