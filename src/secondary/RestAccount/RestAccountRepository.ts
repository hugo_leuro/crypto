import { AccountRepository } from "@/domain/Account/AccountRepository";
import firebase from "firebase/compat";
import {
  getListCryptos,
  RestWallet,
  toWallet,
} from "@/secondary/RestAccount/RestWallet";
import { Wallet } from "@/domain/Account/Wallet";
import { CryptocurrencyRepository } from "@/domain/Cryptocurrency/CryptocurrencyRepository";
import {
  RestCryptocurrency,
  RestCryptocurrencyPrice,
} from "@/secondary/RestAccount/RestCryptocurrencyPrice";
import {
  CryptocurrencyTransaction,
  CryptocurrencyTransactions,
} from "@/domain/Cryptocurrency/CryptocurrencyTransaction";
import {
  toCalculateWallet,
  toCryptoCurrencyTransactions,
} from "@/secondary/RestAccount/RestCryptoCurrencyTransaction";
import Firestore = firebase.firestore.Firestore;
import UserCredential = firebase.auth.UserCredential;
import Auth = firebase.auth.Auth;
import DocumentSnapshot = firebase.firestore.DocumentSnapshot;
import DocumentData = firebase.firestore.DocumentData;

export class RestAccountRepository implements AccountRepository {
  constructor(
    private firestore: Firestore,
    private auth: Auth,
    private cryptoCurrencyRepository: CryptocurrencyRepository
  ) {}

  signIn(email: string, password: string): Promise<boolean> {
    return this.auth
      .signInWithEmailAndPassword(email, password)
      .then((userCredentials: UserCredential) => {
        return true;
      })
      .catch((error: firebase.auth.AuthError) => {
        if (error.code == "auth/user-not-found")
          throw Error(
            "Il semble que votre adresse e-mail soit incorrect. Veuillez essayer à nouveau, s'il vous plaît"
          );
        else if (error.code == "auth/wrong-password")
          throw Error(
            "Il semble que votre mot de passe soit incorrect. Veuillez essayer à nouveau, s'il vous plaît"
          );
        else throw Error(error.message);
      });
  }

  signUp(email: string, password: string): Promise<boolean> {
    return this.auth
      .createUserWithEmailAndPassword(email, password)
      .then((userCredentials: UserCredential) => {
        const user = userCredentials;
        return true;
      })
      .catch(() => {
        return false;
      });
  }

  saveTransactions(
    cryptocurrencyTransactions: CryptocurrencyTransaction[]
  ): Promise<boolean> {
    const data = {
      transactions: cryptocurrencyTransactions,
      wallet: toCalculateWallet(cryptocurrencyTransactions),
    };
    return this.firestore
      .collection("users")
      .doc(this.auth.currentUser!.uid)
      .set(data)
      .then(() => {
        return true;
      })
      .catch(() => {
        return false;
      });
  }

  getTransactions(): Promise<CryptocurrencyTransactions> {
    return this.firestore
      .collection("users")
      .doc(this.auth.currentUser!.uid)
      .get()
      .then((response: DocumentSnapshot<DocumentData>) => {
        if (response.exists)
          return toCryptoCurrencyTransactions(response.data() as any);
        else throw Error("error");
      })
      .catch((error) => {
        throw Error(error);
      });
  }

  getWallet(): Promise<Wallet> {
    return this.firestore
      .collection("users")
      .doc(this.auth.currentUser!.uid)
      .get()
      .then((response: DocumentSnapshot<DocumentData>) => {
        if (response.exists) {
          const list = getListCryptos(response.data()?.wallet as RestWallet);
          let listPrice: RestCryptocurrencyPrice[];

          return this.getListPrice(list).then((data) => {
            listPrice = data;
            return toWallet(response.data()?.wallet as RestWallet, listPrice);
          });
        } else throw Error("error");
      })
      .catch((error) => {
        throw Error(error);
      });
  }

  private getListPrice(list: string): Promise<RestCryptocurrencyPrice[]> {
    return this.cryptoCurrencyRepository
      .getMultiplePrice(list)
      .then((data: RestCryptocurrency) => Object.values(data));
  }
}
