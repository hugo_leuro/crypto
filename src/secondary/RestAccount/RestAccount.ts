import { StoreAccount } from "@/domain/Account/StoreAccount";

export interface RestAccount {
  session: SessionAccount;
}

interface SessionAccount {
  access_token: string;
  refresh_token: string;
}

export const toStoreAccount = (restAccount: any): StoreAccount => ({
  token: restAccount.access_token,
  refreshToken: restAccount.refresh_token,
});
