import {
  CryptocurrencyTransaction,
  CryptocurrencyTransactions,
} from "@/domain/Cryptocurrency/CryptocurrencyTransaction";
import { RestWallet } from "@/secondary/RestAccount/RestWallet";

interface RestCryptoCurrencyTransaction {
  transactions: CryptocurrencyTransaction[];
}

export const toCryptoCurrencyTransactions = (
  restCryptoCurrencyTransaction: RestCryptoCurrencyTransaction
): CryptocurrencyTransactions => ({
  transactions: restCryptoCurrencyTransaction.transactions,
});

const toCalculateTotalSpend = (
  cryptocurrencyTransactions: CryptocurrencyTransaction[]
): number => {
  let total = 0;
  cryptocurrencyTransactions.forEach((transaction) => {
    if (transaction.transactionKind === "crypto_purchase") {
      total = total + Number(transaction.baseAmount);
    }
  });

  return total;
};

export const toCalculateWallet = (
  cryptocurrencyTransactions: CryptocurrencyTransaction[]
): RestWallet => {
  const wallet: RestWallet = {
    crypto: [],
    totalSpend: toCalculateTotalSpend(cryptocurrencyTransactions),
  };

  const cryptos: string[] = [];

  cryptocurrencyTransactions.forEach((transaction) => {
    cryptos.push(transaction.cryptoCurrency);
  });
  const cryptosSort = [...new Set(cryptos)];
  cryptosSort.forEach((crypto) => {
    const cryptoCurrency = { name: crypto, total: 0 };
    cryptocurrencyTransactions.forEach((transaction) => {
      if (
        transaction.cryptoCurrency == crypto &&
        transaction.transactionKind != "crypto_earn_program_created"
      ) {
        if (
          Math.sign(cryptoCurrency.total + Number(transaction.amount)) === -1
        ) {
          cryptoCurrency.total = 0;
        } else {
          cryptoCurrency.total =
            cryptoCurrency.total + Number(transaction.amount);
        }
      }
      if (transaction.type.split(" ->")[0] === crypto) {
        if (
          Math.sign(Number(transaction.amount) + cryptoCurrency.total) === -1
        ) {
          cryptoCurrency.total = 0;
        } else {
          cryptoCurrency.total =
            Number(transaction.amount) + cryptoCurrency.total;
        }
      }

      if (transaction.type.split(" -> ")[1] === crypto) {
        if (
          Math.sign(cryptoCurrency.total + Number(transaction.toAmount)) === -1
        ) {
          cryptoCurrency.total = 0;
        } else {
          cryptoCurrency.total =
            cryptoCurrency.total + Number(transaction.toAmount);
        }
      }
    });
    wallet.crypto.push(cryptoCurrency);
  });
  return wallet;
};
