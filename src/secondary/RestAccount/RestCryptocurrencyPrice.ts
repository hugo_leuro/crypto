export interface RestCryptocurrency {
  [key: number]: RestCryptocurrencyPrice;
}

export interface RestCryptocurrencyPrice {
  EUR: number;
}
