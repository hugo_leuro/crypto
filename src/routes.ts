import { RouteConfig, RouterOptions } from "vue-router";
import { SignUpVue } from "@/primary/components/signUp";
import { SignInVue } from "@/primary/components/signIn";
import { WalletPageVue } from "@/primary/pages/walletPage";
import { DashboardPageVue } from "@/primary/pages/dashboardPage";
import { TransactionPageVue } from "@/primary/pages/transactionPage";
import { TrackPageVue } from "@/primary/pages/trackPage";
import { LayoutMainVue } from "@/primary/layouts/layoutMain";

const routes: RouteConfig[] = [
  {
    path: "/",
    name: "",
    component: LayoutMainVue,
    meta: {
      requiresAuth: true,
    },
    children: [
      {
        path: "",
        name: "dashboard",
        component: DashboardPageVue,
      },
      {
        path: "/wallet",
        name: "wallet",
        component: WalletPageVue,
      },
      {
        path: "/transaction",
        name: "transaction",
        component: TransactionPageVue,
      },
      {
        path: "/track",
        name: "track",
        component: TrackPageVue,
      },
    ],
  },
  {
    path: "/inscription",
    name: "signup",
    component: SignUpVue,
  },
  {
    path: "/connexion",
    name: "signin",
    component: SignInVue,
  },
];

export const routerOptions: RouterOptions = {
  mode: "history",
  routes,
};
