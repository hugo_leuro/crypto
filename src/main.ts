import Vue from "vue";
import { AppVue } from "@/primary/components/app";
import { createRouter } from "@/config/router";
import axios, { AxiosInstance } from "axios";
import { RestAccountRepository } from "@/secondary/RestAccount/RestAccountRepository";
import { createStore } from "@/secondary/Store/StoreVuex";
import { LocaleStoreRepository } from "@/secondary/Store/LocaleStoreRepository";
import { RestCryptocurrencyRepository } from "@/secondary/RestCryptocurrency/RestCryptocurrencyRepository";
// Import the functions you need from the SDKs you need
import { auth, db } from "./firebase";
import ToastCloserBus from "@/primary/components/toast/ToastCloserBus";
import ToastOpenerBus from "@/primary/components/toast/ToastOpenerBus";

Vue.config.productionTip = false;

const router = createRouter();

const basicAxiosInstance: AxiosInstance = axios.create({
  baseURL: process.env.VUE_APP_BASE_API_CRYPTO,
  headers: {
    authorization: `Apikey ${process.env.VUE_APP_BASE_API_CRYPTO_TOKEN}`,
  },
});

const store = createStore();

const localeStoreRepository = new LocaleStoreRepository(store);
const cryptocurrencyRepository = new RestCryptocurrencyRepository(
  db,
  auth,
  basicAxiosInstance
);
const accountRepository = new RestAccountRepository(
  db,
  auth,
  cryptocurrencyRepository
);
let app: Vue;

const vueBus = new Vue();
const toastCloserBus = new ToastCloserBus(vueBus);
const toastOpenerBus = new ToastOpenerBus(vueBus);

auth.onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      el: "#app",
      router,
      provide: {
        localeStoreRepository: () => localeStoreRepository,
        accountRepository: () => accountRepository,
        cryptocurrencyRepository: () => cryptocurrencyRepository,
        toastCloserBus: () => toastCloserBus,
        toastOpenerBus: () => toastOpenerBus,
      },
      render: (h) => h(AppVue),
    }).$mount("#app");
  }
});
