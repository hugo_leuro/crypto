import { Component, Inject, Vue } from "vue-property-decorator";
import { MenuVue } from "@/primary/components/menu";
import Toast from "@/primary/components/toast/Toast";
import ToastCloserBus from "@/primary/components/toast/ToastCloserBus";
import ToastOpenerBus from "@/primary/components/toast/ToastOpenerBus";
import { ToastVue } from "@/primary/components/toast";

@Component({
  components: {
    MenuVue,
    ToastVue,
  },
})
export default class AppComponent extends Vue {
  @Inject()
  private toastOpenerBus!: () => ToastOpenerBus;

  @Inject()
  private toastCloserBus!: () => ToastCloserBus;

  toastId = 0;
  toastOpenedId = 0;
  toastClosedId = 0;
  toastList: Toast[] = [];

  get isNotSignInProcess() {
    return this.$route.name != "signin" && this.$route.name != "signup";
  }

  mounted() {
    this.toastOpenedId = this.toastOpenerBus().subscribe(this.addToast);
    this.toastClosedId = this.toastCloserBus().subscribe(this.closeToast);
  }

  private addToast(toast?: Toast) {
    toast!.id = this.toastId;
    this.toastList.push(toast!);
    this.toastId++;
  }

  private closeToast(idToast?: number) {
    const index = this.toastList.findIndex((toast) => toast.id == idToast);

    this.toastList.splice(index, 1);
  }
}
