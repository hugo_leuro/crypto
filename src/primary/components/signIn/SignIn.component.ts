import { Component, Inject, Vue } from "vue-property-decorator";
import { AccountRepository } from "@/domain/Account/AccountRepository";
import ToastOpenerBus from "@/primary/components/toast/ToastOpenerBus";
import { ToastStatus } from "@/primary/components/toast/ToastStatus";
import { ToastIcon } from "@/primary/components/toast/ToastIcon";

@Component
export default class SignInComponent extends Vue {
  @Inject()
  private accountRepository!: () => AccountRepository;

  @Inject()
  private toastOpenerBus!: () => ToastOpenerBus;

  private email = "";
  private password = "";

  signIn() {
    this.accountRepository()
      .signIn(this.email, this.password)
      .then(() => {
        this.$router.push({ name: "dashboard" });
      })
      .catch((error) => {
        this.toastOpenerBus().add({
          status: ToastStatus.ERROR,
          text: error.message,
          img: ToastIcon.ERROR,
        });
      });
  }
}
