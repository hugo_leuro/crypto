import { Component, Inject, Prop, Vue } from "vue-property-decorator";
import { AccountRepository } from "@/domain/Account/AccountRepository";
import { InputCsvTransactionVue } from "@/primary/components/inputCsvTransaction";
import { CryptocurrencyRepository } from "@/domain/Cryptocurrency/CryptocurrencyRepository";
import { CryptocurrencyTransaction } from "@/domain/Cryptocurrency/CryptocurrencyTransaction";

@Component({
  components: {
    InputCsvTransactionVue,
  },
})
export default class TransactionsTableComponent extends Vue {
  @Inject()
  private accountRepository!: () => AccountRepository;

  @Inject()
  private cryptocurrencyRepository!: () => CryptocurrencyRepository;

  @Prop()
  private transactions!: CryptocurrencyTransaction[];
}
