import TransactionsTableComponent from "./TransactionsTable.component";
import TransactionsTableVue from "./TransactionsTable.vue";

export { TransactionsTableComponent, TransactionsTableVue };
