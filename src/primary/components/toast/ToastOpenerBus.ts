import Toast from "./Toast";

import { Bus } from "@/primary/Bus";

export default class ToastOpenerBus extends Bus<Toast> {
  constructor(bus: Vue) {
    super(bus, "toastOpenerBus");
  }
}
