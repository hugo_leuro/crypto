import { Component, Inject, Prop, Vue } from "vue-property-decorator";

import Toast from "./Toast";
import ToastCloserBus from "./ToastCloserBus";

@Component
export default class ToastComponent extends Vue {
  @Prop({ required: true })
  toast!: Toast;

  @Inject()
  private toastCloserBus!: () => ToastCloserBus;

  created() {
    this.setToastTimeOut();
  }

  setToastTimeOut() {
    setTimeout(this.closeToast, 5000);
  }

  get statusClass() {
    return this.toast.status ? "-" + this.toast.status : "";
  }

  get hasImg() {
    if (this.toast.img == undefined) return false;
    const img = this.toast.img;
    return img;
  }

  closeToast() {
    this.toastCloserBus().add(this.toast.id);
  }
}
