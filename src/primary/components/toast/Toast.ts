export default interface Toast {
  id?: number;
  status?: string;
  text?: string;
  img?: string;
}
