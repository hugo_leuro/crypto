import { Bus } from "@/primary/Bus";

export default class ToastCloserBus extends Bus<number> {
  constructor(bus: Vue) {
    super(bus, "toastCloserBus");
  }
}
