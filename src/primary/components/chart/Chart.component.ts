import { Component, Prop, Vue } from "vue-property-decorator";
import Chart from "chart.js/auto";
import { CryptocurrencyHistorical } from "@/domain/Cryptocurrency/CryptocurrencyHistorical";

@Component({
  components: {},
})
export default class ChartComponent extends Vue {
  @Prop()
  id!: string;

  @Prop()
  datas!: CryptocurrencyHistorical[];

  dates: Date[] = [];
  prices: number[] = [];
  borderColor = "green";

  mounted(): void {
    this.fromDomain();
    this.init();
  }

  private fromDomain() {
    this.datas.forEach((data: CryptocurrencyHistorical) => {
      this.dates.push(data.date);
      this.prices.push(data.price);
    });
    if (this.prices[0] > this.prices[this.prices.length - 1])
      this.borderColor = "red";
  }

  private init() {
    const test = document.getElementById("test");
    new Chart(this.id.toString(), {
      type: "line",
      data: {
        labels: this.dates,
        datasets: [
          {
            data: this.prices,
            borderColor: this.borderColor,
            borderWidth: 0.5,
          },
        ],
      },
      options: {
        plugins: {
          legend: {
            display: false,
          },
          tooltip: {
            enabled: false,
          },
        },
        elements: {
          point: {
            radius: 0,
          },
        },
        scales: {
          y: {
            display: false,
          },
          x: {
            display: false,
          },
        },
      },
    });
  }
}
