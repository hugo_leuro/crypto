import { Component, Inject, Vue } from "vue-property-decorator";
import { AccountRepository } from "@/domain/Account/AccountRepository";
import { InputCsvTransactionVue } from "@/primary/components/inputCsvTransaction";
import { CryptocurrencyRepository } from "@/domain/Cryptocurrency/CryptocurrencyRepository";
import { CryptocurrencyTransaction } from "@/domain/Cryptocurrency/CryptocurrencyTransaction";
import { TransactionsTableVue } from "@/primary/components/transactionsTable";

@Component({
  components: {
    InputCsvTransactionVue,
    TransactionsTableVue,
  },
})
export default class DashboardComponent extends Vue {
  @Inject()
  private accountRepository!: () => AccountRepository;

  @Inject()
  private cryptocurrencyRepository!: () => CryptocurrencyRepository;

  transactions: CryptocurrencyTransaction[] = [];
  totalSpend = 0;

  mounted() {
    this.accountRepository()
      .getTransactions()
      .then((data) => this.fromDomain(data))
      .catch((error) => {
        console.log(error);
      });
  }

  private fromDomain(data: any) {
    this.transactions = data.transactions;
    this.totalSpend = data.totalSpend;
  }
}
