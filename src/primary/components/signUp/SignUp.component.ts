import { Component, Inject, Vue } from "vue-property-decorator";
import { AccountRepository } from "@/domain/Account/AccountRepository";

@Component
export default class SignUpComponent extends Vue {
  @Inject()
  private accountRepository!: () => AccountRepository;

  private email = "";
  private password = "";

  signUp() {
    this.accountRepository()
      .signUp(this.email, this.password)
      .then(() => {
        this.$router.push({ name: "dashboard" });
      });
  }
}
