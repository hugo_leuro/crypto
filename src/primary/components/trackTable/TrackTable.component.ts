import { Component, Prop, Vue } from "vue-property-decorator";
import { MenuVue } from "@/primary/components/menu";
import { CryptocurrencyTrack } from "@/domain/Cryptocurrency/CryptocurrencyTrack";
import { ChartVue } from "@/primary/components/chart";

@Component({
  components: {
    MenuVue,
    ChartVue,
  },
})
export default class TrackTableComponent extends Vue {
  @Prop({ required: true })
  cryptoCurrencies!: CryptocurrencyTrack[];
}
