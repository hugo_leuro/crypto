import { Component, Vue } from "vue-property-decorator";

@Component({
  components: {},
})
export default class MenuComponent extends Vue {
  toggle = false;

  toggleSidebar() {
    this.toggle = !this.toggle;
  }
}
