import { Component, Inject, Vue } from "vue-property-decorator";
import { CryptocurrencyTransactionPrimary } from "@/primary/components/inputCsvTransaction/CryptocurrencyTransactionPrimary";
import { CryptocurrencyTransaction } from "@/domain/Cryptocurrency/CryptocurrencyTransaction";
import { AccountRepository } from "@/domain/Account/AccountRepository";

@Component
export default class InputCsvTransactionComponent extends Vue {
  @Inject()
  private accountRepository!: () => AccountRepository;

  mounted() {
    this.inputOnChange();
  }

  private inputOnChange() {
    document
      .getElementById("fileInput")!
      .addEventListener("change", this.handleFileSelect);
  }

  private handleFileSelect(event: Event): void {
    const reader: FileReader = new FileReader();
    reader.onload = this.handleFileLoad;
    if (event.target)
      reader.readAsText((event.target as HTMLInputElement).files![0]);
  }

  private handleFileLoad(reader: ProgressEvent<FileReader>): void {
    if (reader.target && reader.target.result)
      this.processData(reader.target.result as string);
  }

  private processData(csvFile: string) {
    const transactions: string[] = csvFile.split(/\r\n|\n/);
    transactions.shift();

    const lines: any = [];

    transactions.forEach((item) => {
      const test = item.replace(",,,", ",0,0,");
      const data = test.split(";");
      if (data[0] != "")
        lines.push(Object.assign({}, data[0].split(/,/g).filter(String)));
    });

    this.accountRepository().saveTransactions(
      lines.map((line: CryptocurrencyTransactionPrimary) =>
        this.toCryptocurrencyTransaction(line)
      )
    );
  }

  private toCryptocurrencyTransaction(
    cryptocurrencyTransactionPrimary: CryptocurrencyTransactionPrimary
  ): CryptocurrencyTransaction {
    return {
      date: cryptocurrencyTransactionPrimary["0"],
      type: cryptocurrencyTransactionPrimary["1"],
      cryptoCurrency: cryptocurrencyTransactionPrimary["2"],
      amount: cryptocurrencyTransactionPrimary["3"],
      toCurrency: cryptocurrencyTransactionPrimary["4"],
      toAmount: cryptocurrencyTransactionPrimary["5"],
      baseCurrency: cryptocurrencyTransactionPrimary["6"],
      baseAmount: cryptocurrencyTransactionPrimary["7"],
      baseUsdtAmount: cryptocurrencyTransactionPrimary["8"],
      transactionKind: cryptocurrencyTransactionPrimary["9"],
    };
  }
}
