export interface CryptocurrencyTransactionPrimary {
  0: string;
  1: string;
  2: string;
  3: number;
  4: string;
  5: number;
  6: string;
  7: number;
  8: number;
  9: string;
}
