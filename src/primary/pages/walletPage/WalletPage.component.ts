import { Component, Inject, Vue } from "vue-property-decorator";
import { AccountRepository } from "@/domain/Account/AccountRepository";
import { emptyWallet, Wallet } from "@/domain/Account/Wallet";

@Component
export default class WalletPageComponent extends Vue {
  @Inject()
  private accountRepository!: () => AccountRepository;

  private wallet: Wallet = emptyWallet();
  private total = 0;

  mounted() {
    this.getWallet();
  }

  private getWallet() {
    this.accountRepository()
      .getWallet()
      .then((data) => this.fromDomain(data))
      .catch((error) => {
        console.log(error);
      });
  }

  private fromDomain(data: Wallet) {
    this.wallet = data;

    this.wallet.crypto.forEach((crypto) => {
      this.total += crypto.value;
    });
  }
}
