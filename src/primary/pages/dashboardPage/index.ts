import DashboardPageComponent from "./DashboardPage.component";
import DashboardPageVue from "./DashboardPage.vue";

export { DashboardPageComponent, DashboardPageVue };
