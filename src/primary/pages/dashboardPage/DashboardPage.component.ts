import { Component, Vue } from "vue-property-decorator";
import { MenuVue } from "@/primary/components/menu";

@Component({
  components: {
    MenuVue,
  },
})
export default class DashboardPageComponent extends Vue {}
