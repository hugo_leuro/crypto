import { Component, Inject, Vue } from "vue-property-decorator";
import { TransactionsTableVue } from "@/primary/components/transactionsTable";
import { CryptocurrencyTransaction } from "@/domain/Cryptocurrency/CryptocurrencyTransaction";
import { AccountRepository } from "@/domain/Account/AccountRepository";

@Component({
  components: {
    TransactionsTableVue,
  },
})
export default class TransactionPageComponent extends Vue {
  @Inject()
  private accountRepository!: () => AccountRepository;

  transactions: CryptocurrencyTransaction[] = [];

  mounted() {
    this.accountRepository()
      .getTransactions()
      .then((data) => this.fromDomain(data))
      .catch((error) => {
        console.log(error);
      });
  }

  private fromDomain(data: any) {
    this.transactions = data.transactions;
  }
}
