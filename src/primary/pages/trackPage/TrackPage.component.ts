import { Component, Inject, Vue } from "vue-property-decorator";
import { MenuVue } from "@/primary/components/menu";
import { CryptocurrencyRepository } from "@/domain/Cryptocurrency/CryptocurrencyRepository";
import { CryptocurrencyTrack } from "@/domain/Cryptocurrency/CryptocurrencyTrack";
import { ChartVue } from "@/primary/components/chart";
import { TrackTableVue } from "@/primary/components/trackTable";

@Component({
  components: {
    MenuVue,
    ChartVue,
    TrackTableVue,
  },
})
export default class TrackPageComponent extends Vue {
  @Inject()
  private cryptocurrencyRepository!: () => CryptocurrencyRepository;

  cryptoCurrencies: CryptocurrencyTrack[] = [];

  mounted() {
    this.cryptocurrencyRepository()
      .getMultiplePriceFull("btc,eth,shib,avax")
      .then((data: CryptocurrencyTrack[]) => {
        this.cryptoCurrencies = data;
        console.log(this.cryptoCurrencies);
      })
      .catch((error: any) => {
        console.log(error);
      });
  }
}
