import { Component, Vue } from "vue-property-decorator";
import { MenuVue } from "@/primary/components/menu";

@Component({
  components: {
    MenuVue,
  },
})
export default class LayoutMainComponent extends Vue {}
